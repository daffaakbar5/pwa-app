import React, { useEffect, useState } from "react";
import "./App.css";
import {
  Arrived,
  AssideMenu,
  Browse,
  Clients,
  Footer,
  Header,
  Hero,
  Offline,
} from "./components";
import axios from "axios";
function App() {
  const [items, setItems] = useState([]);
  const [offlineStatus, setOfflineStatus] = useState(!navigator.onLine);

  const handleOfflineStatus = () => {
    setOfflineStatus(!navigator.onLine);
  };

  useEffect(() => {
    // fetch("https://bwacharity.fly.dev/items", {
    //   headers: {
    //     "Content-Type": "application/json",
    //     accept: "application/json",
    //   },
    // })
    //   .then((data) => console.log("data", data.json()))
    //   .catch((err) => console.log(err));

    // setItems(nodes);
    axios
      .get("https://bwacharity.fly.dev/items")
      .then((data) => {
        setItems(data.data.nodes);
        console.log("data", data.data.nodes);
      })
      .catch((err) => console.log("err", err));

    if (!document.querySelector('script[src="/carousel.js"]')) {
      const script = document.createElement("script");
      script.src = "/carousel.js";
      script.async = false;
      document.body.appendChild(script);
    }

    handleOfflineStatus();
    window.addEventListener("online", handleOfflineStatus);
    window.addEventListener("offline", handleOfflineStatus);

    return function () {
      window.addEventListener("online", handleOfflineStatus);
      window.addEventListener("offline", handleOfflineStatus);
    };
  }, [offlineStatus]);

  console.log(items);

  return (
    // <div className="App">
    //   <h1 className="text-3xl font-bold underline">Hello world!</h1>
    // </div>
    <>
      {offlineStatus && <Offline />}
      <Header />
      <Hero />
      <Browse />
      <Arrived items={items} />
      <Clients />
      <AssideMenu />
      <Footer />
    </>
  );
}

export default App;

import Header from "./Header";
import Hero from "./Hero";
import Browse from "./Browse";
import Arrived from "./Arrived";
import Clients from "./Clients";
import AssideMenu from "./AssideMenu";
import Footer from "./Footer";
import Offline from "./Offline";

export { Header, Hero, Browse, Arrived, Clients, AssideMenu, Footer, Offline };
